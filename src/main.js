import 'babel-polyfill';
import Vue from 'vue'
import App from './App.vue'
import VueProgressiveImage from 'vue-progressive-image'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

Vue.use(Viewer, {
  defaultOptions: {
    toolbar: false,
    navbar: true,
    title: false
  }
})
Vue.use(VueProgressiveImage)

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faCommentAlt, faUserAlt, faCommentDots, faCalculator, faCarSide, faMotorcycle, faShuttleVan, faTruck, faTrain, faThumbsDown, faThumbsUp, faCheck, faUserPlus, faUnlockAlt, faCar } from '@fortawesome/free-solid-svg-icons'
import { faUser, faEye, faNewspaper } from '@fortawesome/free-regular-svg-icons'
import { faInstagram, faFacebookF, faGooglePlusG, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

library.add(faSearch, faCommentAlt, faUserAlt, faCommentDots, faCalculator, faCarSide, faMotorcycle, faShuttleVan, faTruck, faTrain, faThumbsDown, faThumbsUp, faCheck, faUserPlus, faUnlockAlt, faCar )
library.add( faUser, faEye, faNewspaper )
library.add( faInstagram, faFacebookF, faGooglePlusG, faTwitter )
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueAwesomeSwiper, /* { default global options } */ )

Vue.config.productionTip = true

import './sass/main.sass'

new Vue({
  render: h => h(App)
}).$mount('#app')
